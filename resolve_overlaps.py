#! /usr/bin/env python3

# Autor: Jan Herec, xherec00
# Popis: Projekt do předmětu BIF, který pomocí dynamického programování řeší problém Weighted Interval Scheduling nad sekvenčními oblastmi DNA  

import sys
from collections import defaultdict
from operator import itemgetter

def main():
    """
    Funkce která pomocí dynamického programování řeší problém Weighted Interval Scheduling nad sekvenčními oblastmi DNA  
    """
    
    # skript vyžaduje jeden parametr, kterým je cesta ke vstupnímu souboru typu gff3, který obsahuje sekvenční oblasti DNA
    if (len(sys.argv) != 2):
        sys.stderr.write("Chyba: skript vyžaduje jeden vstupní parametr, kterým je cesta ke vstupnímu souboru.\n")
        sys.exit(1)

    # získáme cestu ke vstupnímu souboru
    input_filename = sys.argv[1]
    # vstupní soubor otevřeme pro čtení 
    try:
        input_file = open(input_filename, "r")
    except IOError as e:
        sys.stderr.write("Chyba: nepodařilo se otevřít zadaný vstupní soubor pro čtení.\n" + str(e) + "\n")
        sys.exit(1)
    
    # slovník ve tvaru {'<seqid>\t<type>\t<strand>':'[(<source>, <start>, <end>, <score>, <phase>), ...]', ...}
    # kde proměnné jako <seqid> apod. odpovídají sloupcům vstupního souboru typu gff3
    # slovník v tomto tvaru umožňuje seskupit pod daný klíč sekvenční oblasti stejného typu 
    dictionary_seqid_type_strand = defaultdict(list)
    
    # slovník, který pro výše uvedený slovník drží počet n-tic pro daný klíč
    dictionary_of_lengths = {}
    
    # načteme si obsah vstupního souboru do slovníkové struktury
    try:
        # načítáme vstupní soubor po řádcích
        for line in input_file:
            # prázdné řádky a řádky začínající # přeskočíme
            if line.startswith('#') or line.startswith('\n'): 
                continue
            
            # získáme sloupce z řádku
            gff3_columns = line.split("\t")  
            # klíč ve slovníku je tvořen: "<seqid>\t<type>\t<strand>"
            key = gff3_columns[0] + "\t" + gff3_columns[2] + "\t" + gff3_columns[6]
            # danému klíči odpovídá ve slovníku list ntic, a my do listu k danému klíči přidáme položku, kterou je n-tice (<source>, <start>, <end>, <score>, <phase>)
            # musíme si dát pozor na to, že dle definice souboru gff3 jsou sloupce <start> a <end> celé číslo, sloupec <score> potom reálné číslo
            dictionary_seqid_type_strand[key].append((gff3_columns[1], int(gff3_columns[3]), int(gff3_columns[4]), float(gff3_columns[5]), gff3_columns[7]))
            
            # pro daný klíč přičteme počet n-tic které se k němu váží
            if key in dictionary_of_lengths:
                dictionary_of_lengths[key] = dictionary_of_lengths[key] + 1
            else:
                dictionary_of_lengths[key] = 1
    except IOError as e:
        sys.stderr.write("Chyba: při čtení ze vstupního souboru došlo k chybě.\n" + str(e) + "\n")
        sys.exit(1)
    
    # pro daný klíč ve slovníku: 
    #                            1. seřadíme sekvenční oblasti podle <end>
    #                            2. nalezneme pomocí dynamického programování nejlepší řešení a toto zapíšeme do souboru
    for key, value in dictionary_seqid_type_strand.items():
        #seřadíme sekvenční oblasti podle <end>
        dictionary_seqid_type_strand[key] = sorted(value,key=itemgetter(2))
        
        #vypočítáme prvky pole p, kde na indexu j je uložen takový největší index i (kde i<j) sekvenční oblasti, která je kompatibilní s oblastní na indexu j
        p = []
        p.append(0) # na index 0 vložíme 0, kterou stejně nebudeme později používat, potřebujeme jí jen vycpat začátek pole
        for j in range(0, dictionary_of_lengths[key]):
            # vychozi hodnota je 0, pokud se nenajde lepší hodnota
            p.append(0)
            # hledáme index i, kde 0 <= i < j
            for i in range(j - 1, -1, -1):
                # pokud jsme nalezli nepřekrávající se sekvenční oblasti s indexy i a j, kde 0 < i < j
                if dictionary_seqid_type_strand[key][i][2] < dictionary_seqid_type_strand[key][j][1]:
                    p[j + 1] = i + 1 # přeindexujme interval 0...n-1 na 1...n, kvůli následujícím výpočtům
                    break
        
        # vypočet optimální hodnoty O[n]
        O = []
        O.append(0) # na index 0 vložíme 0
        back = [] # pole které použijeme při backtrackingu, tedy nalezení řešení
        back.append(0) # na index 0 vložíme 0, čímž tento index inicializujeme
        for j in range(1, dictionary_of_lengths[key] + 1):
            # inicializujeme bunku pole O a pole back na indexu j
            O.append(0)
            back.append(0)
            
            # při dynamickém progrmaování používáme rekurenci O[j] = max{O[j − 1], vj + O[p[j]]}.
            # kde vj je <score> pro sekvenční oblast s indexem j
            vj = dictionary_seqid_type_strand[key][j - 1][3]
            if vj + O[p[j]] > O[j - 1]:
                O[j] = vj + O[p[j]]
                back[j] = p[j]
            else:
                O[j] = O[j - 1]
                back[j] = j - 1
            
        #print(O[dictionary_of_lengths[key]]) # tisk optimální hodnoty O[n]

        # získání řešení pomocí backtrackingu
        j = dictionary_of_lengths[key]
        while j > 0:
            if back[j] == p[j]:
                splitted_key = key.split("\t")
                print(splitted_key[0] + "\t" + dictionary_seqid_type_strand[key][j - 1][0] + "\t" +  splitted_key[1] + "\t" + str(dictionary_seqid_type_strand[key][j - 1][1]) + "\t" + str(dictionary_seqid_type_strand[key][j - 1][2]) + "\t" + str(dictionary_seqid_type_strand[key][j - 1][3]) + "\t" +  splitted_key[2] + "\t" + dictionary_seqid_type_strand[key][j - 1][4], end='')
            j = back[j]
        

if __name__ == "__main__":
    main()